﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Collective3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            button7.Click += (obj, args) => {
                button7.Text += "!";
            };
            pictureBox1.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("button 3 works");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Моя кнопка! :)");
            MessageBox.Show("Hello!!!", ":)");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int x = 12;
            MessageBox.Show("Это моя кнопка!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Boom!");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            MessageBox.Show("кнопочка 9");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Shulutkova E.");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
        }
    }
}
